package dev.milorad.rijksdata.presentation.detail.viewmodel

import dev.milorad.rijksdata.RxImmediateSchedulerRule
import dev.milorad.rijksdata.business.ArtObjectDetailModel
import dev.milorad.rijksdata.common.Result
import dev.milorad.rijksdata.entity.ArtObjectDetail
import dev.milorad.rijksdata.presentation.detail.uimodel.ArtObjectDetailUiModel
import dev.milorad.rijksdata.presentation.detail.uimodel.ArtObjectDetailUiModelConverter
import dev.milorad.rijksdata.presentation.detail.view.ArtObjectDetailView
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.anyInt
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ArtObjectDetailViewModelTest {

    companion object {
        const val OBJECT_ID = "123"
    }

    @get:Rule val immediateSchedulerRule = RxImmediateSchedulerRule()

    @Mock private lateinit var view: ArtObjectDetailView
    @Mock private lateinit var model: ArtObjectDetailModel
    @Mock private lateinit var converter: ArtObjectDetailUiModelConverter

    private lateinit var viewModel: ArtObjectDetailViewModel

    @Before
    fun setUp() {
        viewModel = ArtObjectDetailViewModel(model, converter)
    }

    @Test(expected = IllegalArgumentException::class)
    fun onViewCreated_noObjectIdSet() {
        viewModel.onViewCreated(view)
    }

    @Test
    fun onViewCreated() {
        viewModel.artObjectId = OBJECT_ID
        val detail = mockDetail()
        val result = mockResult(detail)
        val uiModel = mockUiModel()

        given(model.getArtObjectDetail(OBJECT_ID)).willReturn(Single.just(result))
        given(converter.convert(detail)).willReturn(uiModel)

        viewModel.onViewCreated(view)

        verify(model).getArtObjectDetail(OBJECT_ID)
        verify(converter).convert(detail)
    }

    @Test
    fun loadData_error() {
        viewModel.artObjectId = OBJECT_ID

        given(model.getArtObjectDetail(OBJECT_ID)).willReturn(Single.error(Error()))

        viewModel.onViewCreated(view)

        verify(model).getArtObjectDetail(OBJECT_ID)
        verify(view).showError(anyInt())
    }


    @Test
    fun onRefreshRequested() {
        viewModel.artObjectId = OBJECT_ID
        val result = mockResult(null)
        given(model.getArtObjectDetail(OBJECT_ID)).willReturn(Single.just(result))

        viewModel.onRefreshRequested()

        verify(model).getArtObjectDetail(OBJECT_ID)
    }

    @Suppress("UNCHECKED_CAST")
    private fun mockResult(detail: ArtObjectDetail?): Result<ArtObjectDetail> {
        return (mock(Result.Success::class.java) as Result.Success<ArtObjectDetail>).apply {
            given(data).willReturn(detail)
        }
    }

    private fun mockDetail(): ArtObjectDetail = mock(ArtObjectDetail::class.java)

    private fun mockUiModel() = mock(ArtObjectDetailUiModel::class.java)

}