package dev.milorad.rijksdata.business

import dev.milorad.rijksdata.data_access.ArtCollectionRepo
import dev.milorad.rijksdata.entity.SortMode
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ArtObjectCollectionModelTest {

    @Mock private lateinit var repo: ArtCollectionRepo

    private lateinit var model: ArtObjectCollectionModel

    @Before
    fun setUp() {
        model = ArtObjectCollectionModel(repo)
    }

    @Test
    fun getArtObjectCollection() {
        model.getArtObjectCollection()

        verify(repo).getArtCollection(0, SortMode.ARTIST)
    }

}