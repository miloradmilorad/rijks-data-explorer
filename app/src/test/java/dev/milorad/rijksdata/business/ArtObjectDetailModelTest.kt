package dev.milorad.rijksdata.business

import dev.milorad.rijksdata.data_access.ArtCollectionRepo
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

private const val OBJECT_ID = "123-HELLO"

@RunWith(MockitoJUnitRunner::class)
class ArtObjectDetailModelTest {

    @Mock private lateinit var repo: ArtCollectionRepo

    private lateinit var model: ArtObjectDetailModel

    @Before
    fun setUp() {
        model = ArtObjectDetailModel(repo)
    }

    @Test
    fun getArtObjectDetail() {
        model.getArtObjectDetail(OBJECT_ID)

        verify(repo).getArtObjectDetail(OBJECT_ID)
    }

}