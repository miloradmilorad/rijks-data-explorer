package dev.milorad.rijksdata.data.converter

import dev.milorad.rijksdata.data.api.response.*
import dev.milorad.rijksdata.entity.ArtObjectBasicInfo
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner

@Suppress("SameParameterValue")
@RunWith(MockitoJUnitRunner::class)
class ArtObjectResponseConverterTest {

    private companion object {
        private const val OBJECT_NUMBER = "123-L"
        private const val OBJECT_TITLE = "Title"
        private const val HEADER_URL = "www.url.com"
        private const val AUTHOR = "Author"
        private const val DESCRIPTION = "Description"
        private val WEB_IMAGE = null
    }

    private val converter: ArtObjectResponseConverter = ArtObjectResponseConverter

    @Test
    fun convertArtCollectionResponse() {
        val artObject = converter.convert(geArtCollectionResponse())[0]

        assertArtObjectBasicInfoAsExpected(artObject)
        assertEquals(HEADER_URL, artObject.headerUrl)
    }

    @Test
    fun convertArtObjectDetailResponse() {
        val objectDetail = converter.convert(getArtDetailResponse())

        assertArtObjectBasicInfoAsExpected(objectDetail)
        assertEquals(DESCRIPTION, objectDetail.description)
        assertEquals(null, objectDetail.headerUrl)
    }

    private fun assertArtObjectBasicInfoAsExpected(artObject: ArtObjectBasicInfo) {
        with(artObject) {
            assertEquals(OBJECT_NUMBER, this.id)
            assertEquals(OBJECT_TITLE, this.title)
            assertEquals(AUTHOR, this.author)
            assertEquals(WEB_IMAGE, this.imageUrl)
        }
    }

    //region Stub building

    private fun getArtDetailResponse(): ArtObjectDetailResponse {
        val item = getArtObject()

        return mock(ArtObjectDetailResponse::class.java).apply {
            given(artObject).willReturn(item)
        }
    }

    private fun getArtObject(): ArtObject {
        return mock(ArtObject::class.java).apply {
            given(objectNumber).willReturn(OBJECT_NUMBER)
            given(title).willReturn(OBJECT_TITLE)
            given(principalOrFirstMaker).willReturn(AUTHOR)
            given(webImage).willReturn(WEB_IMAGE)
            given(description).willReturn(DESCRIPTION)
        }
    }

    private fun geArtCollectionResponse(): ArtCollectionResponse {
        val artObject = getArtObjectsItem(getHeaderImage(HEADER_URL))

        return mock(ArtCollectionResponse::class.java).apply {
            given(artObjects).willReturn(listOf(artObject))
        }
    }

    private fun getHeaderImage(headerUrl: String): HeaderImage {
        return mock(HeaderImage::class.java).apply {
            given(url).willReturn(headerUrl)
        }
    }

    private fun getArtObjectsItem(header: HeaderImage): ArtObjectsItem {
        return mock(ArtObjectsItem::class.java).apply {
            given(objectNumber).willReturn(OBJECT_NUMBER)
            given(title).willReturn(OBJECT_TITLE)
            given(principalOrFirstMaker).willReturn(AUTHOR)
            given(webImage).willReturn(WEB_IMAGE)
            given(headerImage).willReturn(header)
        }
    }

    //endregion

}