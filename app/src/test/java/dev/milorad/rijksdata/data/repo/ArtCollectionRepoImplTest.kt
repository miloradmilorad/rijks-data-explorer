package dev.milorad.rijksdata.data.repo

import dev.milorad.rijksdata.common.ErrorMapper
import dev.milorad.rijksdata.data.api.response.ArtCollectionResponse
import dev.milorad.rijksdata.data.api.response.ArtObjectDetailResponse
import dev.milorad.rijksdata.data.converter.ArtObjectResponseConverter
import dev.milorad.rijksdata.data.service.ArtCollectionService
import dev.milorad.rijksdata.data_access.ArtCollectionRepo.Companion.PAGE_SIZE
import dev.milorad.rijksdata.entity.SortMode
import io.reactivex.Observable
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ArtCollectionRepoImplTest {

    companion object {
        const val SORT_MODE = SortMode.ACHRONOLOGIC
        const val PAGE = 1
        const val OBJECT_ID = "123"
    }

    @Mock private lateinit var errorMapper: ErrorMapper
    @Mock private lateinit var converter: ArtObjectResponseConverter
    @Mock private lateinit var service: ArtCollectionService

    private lateinit var repo: ArtCollectionRepoImpl

    @Before
    fun setUp() {
        repo = ArtCollectionRepoImpl(service, converter, errorMapper)
    }

    @Test
    fun getArtCollection_success() {
        val response = mock(ArtCollectionResponse::class.java)
        given(service.getArtCollection(PAGE, PAGE_SIZE, SORT_MODE)).willReturn(Observable.just(response))

        repo.getArtCollection(PAGE, SORT_MODE).test()

        verify(service).getArtCollection(1, PAGE_SIZE, SortMode.ACHRONOLOGIC)
        verify(converter).convert(response)
        verifyNoInteractions(errorMapper)
    }

    @Test
    fun getArtCollection_failure() {
        val error = Error()
        given(service.getArtCollection(PAGE, PAGE_SIZE, SORT_MODE)).willReturn(Observable.error(error))

        repo.getArtCollection(PAGE, SORT_MODE).test()

        verify(service).getArtCollection(1, PAGE_SIZE, SortMode.ACHRONOLOGIC)
        verifyNoInteractions(converter)
        verify(errorMapper).map(error)
    }

    @Test
    fun getArtObjectDetail_success() {
        val response = mock(ArtObjectDetailResponse::class.java)
        given(service.getArtObjectDetail(OBJECT_ID)).willReturn(Single.just(response))

        repo.getArtObjectDetail(OBJECT_ID).test()

        verify(service).getArtObjectDetail(OBJECT_ID)
        verify(converter).convert(response)
    }

    @Test
    fun getArtObjectDetail_failure() {
        val error = Error()
        given(service.getArtObjectDetail(OBJECT_ID)).willReturn(Single.error(error))

        repo.getArtObjectDetail(OBJECT_ID).test()

        verify(service).getArtObjectDetail(OBJECT_ID)
        verifyNoInteractions(converter)
        verify(errorMapper).map(error)
    }

}