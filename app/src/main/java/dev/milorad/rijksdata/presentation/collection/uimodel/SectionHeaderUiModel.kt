package dev.milorad.rijksdata.presentation.collection.uimodel

import dev.milorad.rijksdata.ui.common.UiModel

/**
 * A [UiModel] for a section header with a title.
 */
data class SectionHeaderUiModel(val title: String, override val id: String = title) : UiModel