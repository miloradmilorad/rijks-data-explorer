package dev.milorad.rijksdata.presentation.detail.viewmodel

import androidx.databinding.Bindable
import dev.milorad.rijksdata.BR
import dev.milorad.rijksdata.R
import dev.milorad.rijksdata.business.ArtObjectDetailModel
import dev.milorad.rijksdata.common.Result
import dev.milorad.rijksdata.common.applySchedulersSingle
import dev.milorad.rijksdata.common.showLoadingUntilTerminationSingle
import dev.milorad.rijksdata.entity.ArtObjectDetail
import dev.milorad.rijksdata.presentation.common.BaseViewModel
import dev.milorad.rijksdata.presentation.detail.uimodel.ArtObjectDetailUiModel
import dev.milorad.rijksdata.presentation.detail.uimodel.ArtObjectDetailUiModelConverter
import dev.milorad.rijksdata.presentation.detail.view.ArtObjectDetailView
import io.reactivex.Single

class ArtObjectDetailViewModel(
    private val model: ArtObjectDetailModel,
    private val converter: ArtObjectDetailUiModelConverter
) : BaseViewModel<ArtObjectDetailView>() {

    var artObjectId: String? = null

    @get:Bindable
    var artObjectDetailUiModel: ArtObjectDetailUiModel? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.artObjectDetailUiModel)
        }

    override fun onViewCreated(view: ArtObjectDetailView) {
        super.onViewCreated(view)

        loadData()
    }

    fun onRefreshRequested() {
        loadData()
    }

    private fun loadData() {
        loadObjectDetail(artObjectId ?: throw IllegalArgumentException("Set id of the object before onViewCreated."))
    }

    private fun loadObjectDetail(objectId: String) {
        disposable {
            model.getArtObjectDetail(objectId)
                .compose(showLoadingUntilTerminationSingle(this))
                .compose(applySchedulersSingle())
                .subscribe({
                    when (it) {
                        is Result.Success -> transformToUiModel(it.data)
                        is Result.Error -> view?.showError(R.string.unknown_error_message)
                    }
                }, {
                    view?.showError(R.string.unknown_error_message)
                })
        }
    }

    private fun transformToUiModel(artObject: ArtObjectDetail) {
        disposable {
            Single.just(artObject)
                .compose(showLoadingUntilTerminationSingle(this))
                .compose(applySchedulersSingle())
                .subscribe({
                    artObjectDetailUiModel = converter.convert(it)
                }, {
                    view?.showError(R.string.unknown_error_message)
                })
        }
    }

}