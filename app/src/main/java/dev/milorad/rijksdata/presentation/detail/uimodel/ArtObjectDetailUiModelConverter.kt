package dev.milorad.rijksdata.presentation.detail.uimodel

import androidx.annotation.WorkerThread
import dev.milorad.rijksdata.entity.ArtObjectDetail

object ArtObjectDetailUiModelConverter {

    @WorkerThread
    fun convert(artObjectDetail: ArtObjectDetail): ArtObjectDetailUiModel {
        return with(artObjectDetail) {
            ArtObjectDetailUiModel(title, description, imageUrl)
        }
    }

}