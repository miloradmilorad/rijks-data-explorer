package dev.milorad.rijksdata.presentation.common

interface ArtObjectDetailNavigator {

    fun goToArtObjectDetail(id: String)

}