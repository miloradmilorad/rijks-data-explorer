package dev.milorad.rijksdata.presentation.detail.view

import androidx.annotation.MainThread
import dev.milorad.rijksdata.presentation.common.ErrorView
import dev.milorad.rijksdata.presentation.common.View
import dev.milorad.rijksdata.presentation.detail.uimodel.ArtObjectDetailUiModel

/**
 * [View] for presenting an [ArtObjectDetailUiModel].
 */
@MainThread
interface ArtObjectDetailView : View, ErrorView