package dev.milorad.rijksdata.presentation.collection.view

import androidx.annotation.MainThread
import dev.milorad.rijksdata.presentation.collection.uimodel.ArtObjectUiModel
import dev.milorad.rijksdata.presentation.common.ArtObjectDetailNavigator
import dev.milorad.rijksdata.presentation.common.ErrorView
import dev.milorad.rijksdata.presentation.common.View
import dev.milorad.rijksdata.ui.common.UiModel

/**
 * [View] for presenting a collection of [ArtObjectUiModel].
 */
@MainThread
interface ArtObjectCollectionView : View, ErrorView, ArtObjectDetailNavigator {

    fun setItems(items: List<UiModel>)

}