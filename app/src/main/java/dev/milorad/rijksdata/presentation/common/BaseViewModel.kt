package dev.milorad.rijksdata.presentation.common

import androidx.annotation.CallSuper
import androidx.databinding.Bindable
import dev.milorad.rijksdata.BR
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * A [BaseViewModel] for the app view models with binding support, handling some common operations.
 */
open class BaseViewModel<T : View> : ObservableViewModel() {

    /**
     * The [View] connected to this ViewModel.
     */
    var view: T? = null

    /**
     * Whether there is content loading on the screen right now.
     * To be bound to a loading indicator.
     */
    @get:Bindable
    var loading = false
        set(loading) {
            field = loading
            notifyPropertyChanged(BR.loading)
        }

    /**
     * A collection of [Disposable]s to dispose of in [onViewDestroyed].
     */
    private val disposables: CompositeDisposable = CompositeDisposable()

    /**
     * Called when the view is ready to show data.
     */
    @CallSuper
    open fun onViewCreated(view: T) {
        this.view = view
    }

    /**
     * Called when the view is off the screen.
     */
    @CallSuper
    open fun onViewDestroyed() {
        disposables.clear()
        view = null
    }

    /**
     * Adds the [disposable] to the [composite].
     */
    fun disposable(composite: CompositeDisposable = this.disposables, disposable: () -> Disposable): Disposable {
        return disposable().also {
            composite.add(it)
        }
    }

}