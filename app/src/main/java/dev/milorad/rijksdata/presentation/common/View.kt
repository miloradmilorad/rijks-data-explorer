package dev.milorad.rijksdata.presentation.common

/**
 * Marker interface for views in the app.
 */
interface View