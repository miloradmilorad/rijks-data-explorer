package dev.milorad.rijksdata.presentation.collection.uimodel

import dev.milorad.rijksdata.ui.common.UiModel

data class ArtObjectUiModel(
    override val id: String,
    val title: String,
    val author: String,
    val imageUrl: String?,
    val headerUrl: String?,
    val goToDetail: () -> Unit
) : UiModel