package dev.milorad.rijksdata.presentation.detail.uimodel

data class ArtObjectDetailUiModel(
    val title: String,
    val description: String?,
    val imageUrl: String?
)