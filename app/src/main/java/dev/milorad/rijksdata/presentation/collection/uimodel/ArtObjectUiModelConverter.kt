package dev.milorad.rijksdata.presentation.collection.uimodel

import androidx.annotation.WorkerThread
import dev.milorad.rijksdata.entity.ArtObject
import dev.milorad.rijksdata.presentation.common.View

/**
 * Converts [ArtObject] entities to [ArtObjectUiModel] to use in [View]s.
 */
object ArtObjectUiModelConverter {

    @WorkerThread
    fun convert(entity: ArtObject, goToDetail: () -> Unit): ArtObjectUiModel {
        return with(entity) {
            ArtObjectUiModel(id, title, author, imageUrl, entity.headerUrl, goToDetail)
        }
    }

}