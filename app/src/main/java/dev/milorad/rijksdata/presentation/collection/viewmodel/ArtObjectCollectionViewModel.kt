package dev.milorad.rijksdata.presentation.collection.viewmodel

import androidx.annotation.WorkerThread
import dev.milorad.rijksdata.R
import dev.milorad.rijksdata.business.ArtObjectCollectionModel
import dev.milorad.rijksdata.common.Result
import dev.milorad.rijksdata.common.applySchedulersObservable
import dev.milorad.rijksdata.common.showLoadingUntilTerminationObservable
import dev.milorad.rijksdata.entity.ArtObject
import dev.milorad.rijksdata.presentation.collection.uimodel.ArtObjectUiModelConverter
import dev.milorad.rijksdata.presentation.collection.uimodel.SectionHeaderUiModel
import dev.milorad.rijksdata.presentation.collection.view.ArtObjectCollectionView
import dev.milorad.rijksdata.presentation.common.BaseViewModel
import dev.milorad.rijksdata.ui.common.UiModel
import io.reactivex.Observable

class ArtObjectCollectionViewModel(
    private val model: ArtObjectCollectionModel,
    private val uiModelConverter: ArtObjectUiModelConverter
) : BaseViewModel<ArtObjectCollectionView>() {

    override fun onViewCreated(view: ArtObjectCollectionView) {
        super.onViewCreated(view)
        loadData()
    }

    fun onRefreshRequested() {
        loadData()
    }

    private fun loadData() {
        disposable {
            model.getArtObjectCollection()
                .compose(showLoadingUntilTerminationObservable(this))
                .compose(applySchedulersObservable())
                .subscribe({
                    when (it) {
                        is Result.Success -> transformToUiModels(view, it.data)
                        is Result.Error -> handleExpectedError(it)
                    }
                }, {
                    handleFatalError()
                })
        }
    }

    private fun transformToUiModels(view: ArtObjectCollectionView?, artObjects: List<ArtObject>) {
        disposable {
            Observable.just(artObjects)
                .compose(showLoadingUntilTerminationObservable(this))
                .compose(applySchedulersObservable())
                .map { convertToUiModelsWithHeaders(it) }
                .subscribe({
                    view?.setItems(it)
                }, {
                    handleFatalError()
                })
        }
    }

    @WorkerThread
    private fun convertToUiModelsWithHeaders(artObjects: List<ArtObject>): List<UiModel> {
        return artObjects.groupBy { it.author }
            .map { entry: Map.Entry<String, List<ArtObject>> ->
                val header = SectionHeaderUiModel(entry.key)
                val items = entry.value.map { uiModelConverter.convert(it) { goToDetail(it.id) } }

                listOf(header, *items.toTypedArray())
            }
            .fold(emptyList(), { a, b -> a + b })
    }

    private fun goToDetail(id: String) {
        view?.goToArtObjectDetail(id)
    }

    private fun handleFatalError() {
        view?.showError(R.string.unknown_error_message)
    }

    private fun handleExpectedError(error: Result.Error<List<ArtObject>>) {
        //TODO Handle expected errors gracefully
        handleFatalError()
    }

}