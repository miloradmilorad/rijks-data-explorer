package dev.milorad.rijksdata.presentation.common

import androidx.annotation.StringRes

/**
 * Interface for views showing an error message.
 */
interface ErrorView {

    fun showError(@StringRes errorMessage: Int)

}