package dev.milorad.rijksdata.business

import dev.milorad.rijksdata.common.Result
import dev.milorad.rijksdata.data_access.ArtCollectionRepo
import dev.milorad.rijksdata.entity.ArtObject
import dev.milorad.rijksdata.entity.SortMode
import io.reactivex.Observable

/**
 * Model incapsulating the business rules for showing a collection of [ArtObject].
 */
class ArtObjectCollectionModel(private val repo: ArtCollectionRepo) {

    private companion object {
        // sort by field we'll use for sectioning the content
        private const val DEFAULT_SORT = SortMode.ARTIST
    }

    //TODO pagination
    private var page: Int = 0

    fun getArtObjectCollection(): Observable<Result<List<ArtObject>>> = repo.getArtCollection(page, DEFAULT_SORT)

}