package dev.milorad.rijksdata.business

import dev.milorad.rijksdata.common.Result
import dev.milorad.rijksdata.data_access.ArtCollectionRepo
import dev.milorad.rijksdata.entity.ArtObjectDetail
import io.reactivex.Single

/**
 * Model incapsulating the business rules for showing an [ArtObjectDetail].
 */
class ArtObjectDetailModel(private val repo: ArtCollectionRepo) {

    fun getArtObjectDetail(id: String): Single<Result<ArtObjectDetail>> = repo.getArtObjectDetail(id)

}