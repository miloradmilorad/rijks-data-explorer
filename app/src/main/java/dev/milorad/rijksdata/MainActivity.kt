package dev.milorad.rijksdata

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import dev.milorad.rijksdata.presentation.common.ArtObjectDetailNavigator
import dev.milorad.rijksdata.ui.collection.ArtObjectCollectionFragment
import dev.milorad.rijksdata.ui.detail.ArtObjectDetailFragment

class MainActivity : AppCompatActivity(), ArtObjectDetailNavigator {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, ArtObjectCollectionFragment.newInstance())
                .commitNow()
        }
    }

    override fun goToArtObjectDetail(id: String) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, ArtObjectDetailFragment.newInstance(id))
            .addToBackStack(null)
            .commit()
    }

}