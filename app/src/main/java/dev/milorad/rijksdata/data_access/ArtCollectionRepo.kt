package dev.milorad.rijksdata.data_access

import dev.milorad.rijksdata.common.Result
import dev.milorad.rijksdata.entity.ArtObject
import dev.milorad.rijksdata.entity.ArtObjectDetail
import io.reactivex.Observable
import io.reactivex.Single

/**
 * A Repository for getting data about [ArtObject]s.
 */
interface ArtCollectionRepo {

    companion object {
        /**
         * Default page size for fetching [ArtObject]s.
         * TODO Determine the right value to use at runtime from screen dimension
         */
        const val PAGE_SIZE = 30
    }

    fun getArtCollection(page: Int, sort: String): Observable<Result<List<ArtObject>>>

    fun getArtObjectDetail(id: String): Single<Result<ArtObjectDetail>>

}