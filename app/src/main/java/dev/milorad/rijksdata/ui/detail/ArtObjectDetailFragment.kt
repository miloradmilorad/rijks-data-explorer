package dev.milorad.rijksdata.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dev.milorad.rijksdata.R
import dev.milorad.rijksdata.common.DependencyLocator
import dev.milorad.rijksdata.databinding.ArtObjectDetailFragmentBinding
import dev.milorad.rijksdata.presentation.detail.view.ArtObjectDetailView
import dev.milorad.rijksdata.presentation.detail.viewmodel.ArtObjectDetailViewModel

private const val ARG_ART_OBJECT_ID = "arg_art_object_πid"

class ArtObjectDetailFragment : Fragment(),
    ArtObjectDetailView {

    companion object {
        fun newInstance(id: String): ArtObjectDetailFragment {
            return ArtObjectDetailFragment().apply {
                arguments = Bundle().apply { putString(ARG_ART_OBJECT_ID, id) }
            }
        }
    }

    private lateinit var viewModel: ArtObjectDetailViewModel
    private lateinit var binding: ArtObjectDetailFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.art_object_detail_fragment, container, false)

        viewModel = ViewModelProvider(this, DependencyLocator.ArtObjectDetailViewModelFactory)
            .get(ArtObjectDetailViewModel::class.java)
            .also {
                it.artObjectId = arguments?.getString(ARG_ART_OBJECT_ID)
                binding.viewModel = it
                it.onViewCreated(this)
            }

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModel.onViewDestroyed()
        binding.unbind()
    }

    override fun showError(errorMessage: Int) {
        Toast.makeText(requireContext(), errorMessage, Toast.LENGTH_SHORT).show()
    }

}