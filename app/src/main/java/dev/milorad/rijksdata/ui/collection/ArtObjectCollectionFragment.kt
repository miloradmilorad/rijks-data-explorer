package dev.milorad.rijksdata.ui.collection

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dev.milorad.rijksdata.R
import dev.milorad.rijksdata.common.DependencyLocator
import dev.milorad.rijksdata.databinding.ArtObjectCollectionFragmentBinding
import dev.milorad.rijksdata.presentation.collection.view.ArtObjectCollectionView
import dev.milorad.rijksdata.presentation.collection.viewmodel.ArtObjectCollectionViewModel
import dev.milorad.rijksdata.presentation.common.ArtObjectDetailNavigator
import dev.milorad.rijksdata.ui.common.UiModel

class ArtObjectCollectionFragment : Fragment(), ArtObjectCollectionView {

    companion object {
        fun newInstance() = ArtObjectCollectionFragment()
    }

    private lateinit var viewModel: ArtObjectCollectionViewModel
    private lateinit var binding: ArtObjectCollectionFragmentBinding

    private var adapter: ArtObjectCollectionAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.art_object_collection_fragment, container, false)

        adapter = ArtObjectCollectionAdapter().apply {
            binding.recyclerView.adapter = this
        }

        viewModel = ViewModelProvider(this, DependencyLocator.ArtObjectCollectionViewModelFactory)
            .get(ArtObjectCollectionViewModel::class.java)
            .also {
                binding.viewModel = it
                it.onViewCreated(this)
            }

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModel.onViewDestroyed()
        binding.unbind()
        adapter = null
    }

    override fun setItems(items: List<UiModel>) {
        adapter?.dataSet = items
    }

    override fun showError(errorMessage: Int) {
        Toast.makeText(requireContext(), errorMessage, Toast.LENGTH_SHORT).show()
    }

    override fun goToArtObjectDetail(id: String) {
        (requireActivity() as? ArtObjectDetailNavigator)?.goToArtObjectDetail(id)
            ?: throw IllegalStateException("Host activity must implement `ArtObjectDetailNavigator` interface.")
    }

}