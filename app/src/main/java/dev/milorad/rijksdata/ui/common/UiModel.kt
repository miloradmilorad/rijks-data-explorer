package dev.milorad.rijksdata.ui.common

/**
 * An interface for all ui models ini the app.
 */
interface UiModel {
    val id: String
}