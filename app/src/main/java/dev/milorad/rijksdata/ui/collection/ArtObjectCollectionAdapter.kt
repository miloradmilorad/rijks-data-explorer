package dev.milorad.rijksdata.ui.collection

import dev.milorad.rijksdata.R
import dev.milorad.rijksdata.presentation.collection.uimodel.ArtObjectUiModel
import dev.milorad.rijksdata.presentation.collection.uimodel.SectionHeaderUiModel
import dev.milorad.rijksdata.ui.common.BoundItemsAdapter
import dev.milorad.rijksdata.ui.common.UiModel

class ArtObjectCollectionAdapter : BoundItemsAdapter<UiModel>() {

    override fun getLayoutId(viewType: Int): Int = viewType

    override fun getItemViewType(position: Int): Int {
        return when (dataSet[position]) {
            is SectionHeaderUiModel -> R.layout.section_header_list_item
            is ArtObjectUiModel -> R.layout.art_object_collection_item
            else -> throw IllegalArgumentException("Cannot bind to unsupported `UiModel` type.")
        }
    }

}