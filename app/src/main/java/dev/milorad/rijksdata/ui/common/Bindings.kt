package dev.milorad.rijksdata.ui.common

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import dev.milorad.rijksdata.common.DependencyLocator

/**
 * Bindings for custom xml attributes using data binding.
 * See [Android data binding documentation](http://developer.android.com/tools/data-binding/guide.html)
 */
object Bindings {

    /**
     * Sets the image fetched from the [url] into the [imageView].
     */
    @JvmStatic
    @BindingAdapter("url")
    fun setImageUrl(imageView: ImageView, url: String?) {
        DependencyLocator.getImageLoader().load(url, imageView)
    }

    /**
     * Sets the [listener] to the  [swipeRefreshLayout].
     */
    @JvmStatic
    @BindingAdapter("onRefresh")
    fun setOnRefreshAction(swipeRefreshLayout: SwipeRefreshLayout, listener: SwipeRefreshLayout.OnRefreshListener) {
        swipeRefreshLayout.setOnRefreshListener(listener)
    }

    /**
     * Triggers showing the [swipeRefreshLayout] loading indicator if [refreshing] is true, hides it otherwise.
     */
    @JvmStatic
    @BindingAdapter("refreshing")
    fun setRefreshing(swipeRefreshLayout: SwipeRefreshLayout, refreshing: Boolean) {
        if (swipeRefreshLayout.isRefreshing != refreshing) {
            swipeRefreshLayout.isRefreshing = refreshing
        }
    }

}