package dev.milorad.rijksdata.ui.common

import android.widget.ImageView
import com.squareup.picasso.Picasso
import dev.milorad.rijksdata.R

/**
 * An [ImageLoader] using Picasso.
 */
object PicassoImageLoader : ImageLoader {

    override fun load(url: String?, imageView: ImageView) {
        Picasso.get().load(url).placeholder(R.drawable.ic_rijksmuseum).into(imageView)
    }
}