package dev.milorad.rijksdata.ui.common

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import dev.milorad.rijksdata.BR
import java.util.*

/**
 * A [RecyclerView.Adapter] for [RecyclerView] ][UiModel] children that use data binding.
 * The xml variable name must be viewModel.
 */
abstract class BoundItemsAdapter<T : UiModel> :
    RecyclerView.Adapter<BoundItemsAdapter.ItemViewHolder>() {

    var dataSet: List<T> = Collections.emptyList()
        set(value) {
            val diffUtilCallback = onCreateDiffUtilCallback(field, value)
            field = value

            diffUtilCallback?.let {
                val result = DiffUtil.calculateDiff(it, true)
                result.dispatchUpdatesTo(this)
            } ?: notifyDataSetChanged()
        }

    @LayoutRes
    abstract fun getLayoutId(viewType: Int): Int

    open fun onCreateDiffUtilCallback(oldItems: List<T>, newItems: List<T>): DiffUtil.Callback? {
        return DefaultDiffCallback(newItems, oldItems)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), getLayoutId(viewType), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.setViewModel(dataSet[position])
    }

    override fun getItemCount(): Int = dataSet.size

    class ItemViewHolder(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {

        fun <T> setViewModel(viewModel: T) {
            binding.setVariable(BR.viewModel, viewModel)
            binding.executePendingBindings()
        }
    }

}

/**
 * A [DiffUtil.Callback] for [UiModel]s that **correctly implement [equals]**.
 */
private class DefaultDiffCallback(
    private val newList: List<UiModel>,
    private val oldList: List<UiModel>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }

}