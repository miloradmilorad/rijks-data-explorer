package dev.milorad.rijksdata.ui.common

import android.widget.ImageView

/**
 * Loads images behind urls into [ImageView]s.
 */
interface ImageLoader {
    /**
     * Loads the image the url points to, into the [ImageView].
     */
    fun load(url: String?, imageView: ImageView)
}