package dev.milorad.rijksdata.common

import dev.milorad.rijksdata.common.Result.Error
import dev.milorad.rijksdata.common.Result.Success
import dev.milorad.rijksdata.entity.ErrorEntity
import io.reactivex.Observable
import io.reactivex.Single

/**
 * Sealed hierarchy representing a result of a request. Wraps the original data in a [Success],
 * or any kind of [ErrorEntity] in an [Error].
 */
sealed class Result<T> {

    data class Success<T>(val data: T) : Result<T>()

    data class Error<T>(val error: ErrorEntity) : Result<T>()

}

/**
 * Utility extension functions to map results and errors of Rx operations to corresponding [Result]s.
 */
fun <T> Single<T>.toResult(errorMapper: ErrorMapper): Single<Result<T>> = this
    .map(fun(it: T): Result<T> = Success(it))
    .onErrorReturn(fun(it: Throwable): Result<T> = Error(errorMapper.map(it)))

fun <T> Observable<T>.toResult(errorMapper: ErrorMapper): Observable<Result<T>> = this
    .map(fun(it: T): Result<T> = Success(it))
    .onErrorReturn(fun(it: Throwable): Result<T> = Error(errorMapper.map(it)))