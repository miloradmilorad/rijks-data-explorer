package dev.milorad.rijksdata.common

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dev.milorad.rijksdata.business.ArtObjectCollectionModel
import dev.milorad.rijksdata.business.ArtObjectDetailModel
import dev.milorad.rijksdata.data.api.RijksDataApi
import dev.milorad.rijksdata.data.converter.ArtObjectResponseConverter
import dev.milorad.rijksdata.data.network.RestClient
import dev.milorad.rijksdata.data.repo.ArtCollectionRepoImpl
import dev.milorad.rijksdata.data.service.ArtCollectionService
import dev.milorad.rijksdata.data.service.ArtCollectionServiceImpl
import dev.milorad.rijksdata.data_access.ArtCollectionRepo
import dev.milorad.rijksdata.presentation.collection.uimodel.ArtObjectUiModelConverter
import dev.milorad.rijksdata.presentation.collection.viewmodel.ArtObjectCollectionViewModel
import dev.milorad.rijksdata.presentation.detail.uimodel.ArtObjectDetailUiModelConverter
import dev.milorad.rijksdata.presentation.detail.viewmodel.ArtObjectDetailViewModel
import dev.milorad.rijksdata.ui.common.ImageLoader
import dev.milorad.rijksdata.ui.common.PicassoImageLoader

/**
 * A dumb service locator for fetching app dependencies.
 * TODO Replace with Dagger2
 */
@Suppress("UNCHECKED_CAST")
object DependencyLocator {

    fun getArtObjectCollectionModel(): ArtObjectCollectionModel = ArtObjectCollectionModel(getArtCollectionRepo())

    fun getArtObjectDetailCollectionModel(): ArtObjectDetailModel = ArtObjectDetailModel(getArtCollectionRepo())

    private fun getArtCollectionRepo(): ArtCollectionRepo =
        ArtCollectionRepoImpl(getArtCollectionService(), getArtObjectConverter(), getErrorMapper())

    private fun getErrorMapper(): ErrorMapper = ErrorMapper()

    private fun getArtObjectConverter(): ArtObjectResponseConverter = ArtObjectResponseConverter

    private fun getArtCollectionService(): ArtCollectionService = ArtCollectionServiceImpl(getRijksDataApi())

    private fun getRijksDataApi(): RijksDataApi = RestClient.rijksDataApi

    fun getArtObjectUiModelConverter(): ArtObjectUiModelConverter = ArtObjectUiModelConverter

    fun getArtObjectDetailConverter(): ArtObjectDetailUiModelConverter = ArtObjectDetailUiModelConverter

    fun getImageLoader(): ImageLoader = PicassoImageLoader

    object ArtObjectCollectionViewModelFactory : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ArtObjectCollectionViewModel(
                getArtObjectCollectionModel(),
                getArtObjectUiModelConverter()
            ) as T
        }
    }

    object ArtObjectDetailViewModelFactory : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ArtObjectDetailViewModel(
                getArtObjectDetailCollectionModel(),
                getArtObjectDetailConverter()
            ) as T
        }
    }

}