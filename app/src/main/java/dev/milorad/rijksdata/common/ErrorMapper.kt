package dev.milorad.rijksdata.common

import android.util.Log
import dev.milorad.rijksdata.entity.ErrorEntity
import retrofit2.HttpException
import java.io.IOException

/**
 * Maps data-layer [Throwable]s to [ErrorEntity].
 * //TODO Expand to cover the most common http errors after testing.
 */
class ErrorMapper {

    fun map(throwable: Throwable): ErrorEntity {
        Log.e(javaClass.simpleName, throwable.toString())

        return when (throwable) {
            is IOException, is HttpException -> ErrorEntity.Network
            else -> ErrorEntity.Unknown
        }
    }

}