package dev.milorad.rijksdata.common

import dev.milorad.rijksdata.presentation.common.BaseViewModel
import dev.milorad.rijksdata.presentation.common.View
import io.reactivex.ObservableTransformer
import io.reactivex.SingleTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Applies the default Schedulers for android.
 * `subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())`
 *
 * @return The ObservableTransformer to pass to compose() in the chain.
 */
fun <T> applySchedulersObservable(): ObservableTransformer<T, T> {
    return ObservableTransformer { upstream ->
        upstream.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}

/**
 * Applies the default Schedulers for android.
 * subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
 *
 * @return The SingleTransformer to pass to compose() in the chain.
 */
fun <T> applySchedulersSingle(): SingleTransformer<T, T> {
    return SingleTransformer { upstream ->
        upstream.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}

/**
 * Toggles the loading property of the view model until completion.
 *
 * @param viewModel The ViewModel to toggle the loading of.
 * @return The SingleTransformer to pass to compose() in the chain.
 */
fun <T> showLoadingUntilTerminationObservable(viewModel: BaseViewModel<out View>): ObservableTransformer<T, T> {
    return ObservableTransformer { upstream ->
        upstream
            .doOnSubscribe { viewModel.loading = true }
            .doOnError { viewModel.loading = false }
            .doOnComplete { viewModel.loading = false }
            .doOnDispose { viewModel.loading = false }
    }
}

/**
 * Toggles the loading property of the view model until completion.
 *
 * @param viewModel The ViewModel to toggle the loading of.
 * @return The SingleTransformer to pass to compose() in the chain.
 */
fun <T> showLoadingUntilTerminationSingle(viewModel: BaseViewModel<out View>): SingleTransformer<T, T> {
    return SingleTransformer { upstream ->
        upstream
            .doOnSubscribe { p -> viewModel.loading = true }
            .doOnSuccess { p -> viewModel.loading = false }
            .doOnError { p -> viewModel.loading = false }
            .doOnDispose { viewModel.loading = false }
    }
}