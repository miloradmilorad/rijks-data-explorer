package dev.milorad.rijksdata.entity

interface ArtObjectBasicInfo {
    val id: String
    val title: String
    val author: String
    val imageUrl: String?
    val headerUrl: String?
}