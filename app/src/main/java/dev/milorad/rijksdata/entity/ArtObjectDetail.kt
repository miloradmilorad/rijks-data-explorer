package dev.milorad.rijksdata.entity

data class ArtObjectDetail(
    private val artObject: ArtObject,
    val description: String?
) : ArtObjectBasicInfo by artObject