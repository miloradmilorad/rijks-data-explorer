package dev.milorad.rijksdata.entity

import androidx.annotation.StringDef
import dev.milorad.rijksdata.entity.SortMode.Companion.ACHRONOLOGIC
import dev.milorad.rijksdata.entity.SortMode.Companion.ARTIST
import dev.milorad.rijksdata.entity.SortMode.Companion.ARTIST_DESC
import dev.milorad.rijksdata.entity.SortMode.Companion.CHRONOLOGIC
import dev.milorad.rijksdata.entity.SortMode.Companion.OBJECT_TYPE
import dev.milorad.rijksdata.entity.SortMode.Companion.RELEVANCE

/**
 * Sorting options for [ArtObject]s.
 */
@StringDef(RELEVANCE, OBJECT_TYPE, CHRONOLOGIC, ACHRONOLOGIC, ARTIST, ARTIST_DESC)
annotation class SortMode {

    companion object {
        const val RELEVANCE = "relevance"
        const val OBJECT_TYPE = "objecttype"
        const val CHRONOLOGIC = "chronologic"
        const val ACHRONOLOGIC = "achronologic"
        const val ARTIST = "artist"
        const val ARTIST_DESC = "artistdesc"
    }

}