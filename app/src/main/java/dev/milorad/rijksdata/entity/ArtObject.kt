package dev.milorad.rijksdata.entity

data class ArtObject(
    override val id: String,
    override val title: String,
    override val author: String,
    override val imageUrl: String?,
    override val headerUrl: String?
) : ArtObjectBasicInfo