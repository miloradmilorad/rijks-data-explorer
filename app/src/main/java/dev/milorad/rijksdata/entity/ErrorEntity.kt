package dev.milorad.rijksdata.entity

sealed class ErrorEntity {

    object Network : ErrorEntity()

    object Unknown : ErrorEntity()

}