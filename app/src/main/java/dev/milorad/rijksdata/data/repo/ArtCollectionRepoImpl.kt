package dev.milorad.rijksdata.data.repo

import dev.milorad.rijksdata.common.ErrorMapper
import dev.milorad.rijksdata.common.Result
import dev.milorad.rijksdata.common.toResult
import dev.milorad.rijksdata.data.converter.ArtObjectResponseConverter
import dev.milorad.rijksdata.data.service.ArtCollectionService
import dev.milorad.rijksdata.data_access.ArtCollectionRepo
import dev.milorad.rijksdata.data_access.ArtCollectionRepo.Companion.PAGE_SIZE
import dev.milorad.rijksdata.entity.ArtObject
import dev.milorad.rijksdata.entity.ArtObjectDetail
import io.reactivex.Observable
import io.reactivex.Single

/**
 * An [ArtCollectionRepo] relying on a [ArtCollectionService].
 * TODO: Add a caching layer on disk to not hit the network each time.
 */
class ArtCollectionRepoImpl(
    private val artCollectionService: ArtCollectionService,
    private val converter: ArtObjectResponseConverter,
    private val errorMapper: ErrorMapper
) : ArtCollectionRepo {

    override fun getArtCollection(page: Int, sort: String): Observable<Result<List<ArtObject>>> {
        return artCollectionService
            .getArtCollection(page, PAGE_SIZE, sort)
            .map { converter.convert(it) }
            .toResult(errorMapper)
    }

    override fun getArtObjectDetail(id: String): Single<Result<ArtObjectDetail>> {
        return artCollectionService
            .getArtObjectDetail(id)
            .map { converter.convert(it) }
            .toResult(errorMapper)
    }

}