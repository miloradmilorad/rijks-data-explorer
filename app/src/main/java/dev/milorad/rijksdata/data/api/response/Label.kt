package dev.milorad.rijksdata.data.api.response

data class Label(
    val date: String?,
    val notes: String?,
    val description: String?,
    val title: String?,
    val makerLine: String?
)