package dev.milorad.rijksdata.data.api.response

data class Acquisition(
    val date: String?,
    val method: String?,
    val creditLine: String?
)