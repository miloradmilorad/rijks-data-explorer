package dev.milorad.rijksdata.data.api.response

data class DetailLinks(
    val search: String?
)