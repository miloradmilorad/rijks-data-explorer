package dev.milorad.rijksdata.data.network

import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import dev.milorad.rijksdata.data.api.RijksDataApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private const val BASE_URL = "https://www.rijksmuseum.nl/api/"
private const val LOGGING_ENABLED = false

@Suppress("ConstantConditionIf")
object RestClient {

    private val retrofit by lazy {
        val httpClient = OkHttpClient.Builder()

        if (LOGGING_ENABLED) {
            HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
                httpClient.addInterceptor(this)
            }
        }

        Retrofit.Builder().baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(httpClient.build())
            .build()
    }

    val rijksDataApi: RijksDataApi by lazy {
        retrofit.create(RijksDataApi::class.java)
    }

}