package dev.milorad.rijksdata.data.api.response

data class AdlibOverrides(
    val titel: Any?,
    val etiketText: Any?,
    val maker: Any?
)