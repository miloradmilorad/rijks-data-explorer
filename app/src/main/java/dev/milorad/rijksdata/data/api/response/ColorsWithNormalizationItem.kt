package dev.milorad.rijksdata.data.api.response

data class ColorsWithNormalizationItem(
    val normalizedHex: String?,
    val originalHex: String?
)