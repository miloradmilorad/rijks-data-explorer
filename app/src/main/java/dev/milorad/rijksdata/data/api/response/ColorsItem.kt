package dev.milorad.rijksdata.data.api.response

data class ColorsItem(
    val percentage: Int?,
    val hex: String?
)