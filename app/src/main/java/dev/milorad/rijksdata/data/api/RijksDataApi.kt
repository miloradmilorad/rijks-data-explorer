package dev.milorad.rijksdata.data.api

import androidx.annotation.IntRange
import androidx.annotation.StringDef
import dev.milorad.rijksdata.data.api.Culture.Companion.EN
import dev.milorad.rijksdata.data.api.Culture.Companion.NL
import dev.milorad.rijksdata.data.api.response.ArtCollectionResponse
import dev.milorad.rijksdata.data.api.response.ArtObjectDetailResponse
import dev.milorad.rijksdata.entity.SortMode
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Rijksmuseum art collection API definition.
 * Docs [here](https://data.rijksmuseum.nl/object-metadata/api).
 *
 */
interface RijksDataApi {

    @GET("{culture}/collection")
    fun getArtCollection(
        @Path("culture") @Culture culture: String,
        @Query("p") @IntRange(from = 0) page: Int,
        @Query("ps") @IntRange(from = 1, to = 100) pageSize: Int,
        @Query("s") @SortMode sort: String,
        @Query("key") apiKey: String
    ): Observable<ArtCollectionResponse>

    @GET("{culture}/collection/{object-number}")
    fun getArtObjectDetail(
        @Path("culture") @Culture culture: String,
        @Path("object-number") id: String,
        @Query("key") apiKey: String
    ): Single<ArtObjectDetailResponse>

}

@StringDef(NL, EN)
annotation class Culture {

    companion object {
        const val NL = "nl"
        const val EN = "en"
    }

}