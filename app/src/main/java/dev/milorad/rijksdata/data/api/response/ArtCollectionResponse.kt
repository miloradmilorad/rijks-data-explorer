package dev.milorad.rijksdata.data.api.response

data class ArtCollectionResponse(
    val artObjects: List<ArtObjectsItem>,
    val countFacets: CountFacets?,
    val count: Int?,
    val elapsedMilliseconds: Int?,
    val facets: List<FacetsItem?>?
)