package dev.milorad.rijksdata.data.api.response

data class PrincipalMakersItem(
    val placeOfBirth: String?,
    val occupation: List<String?>?,
    val dateOfDeath: String?,
    val roles: List<String?>?,
    val productionPlaces: List<String?>?,
    val dateOfBirth: String?,
    val placeOfDeath: String?,
    val biography: Any?,
    val dateOfDeathPrecision: Any?,
    val qualification: Any?,
    val nationality: String?,
    val unFixedName: String?,
    val name: String?,
    val dateOfBirthPrecision: Any?
)