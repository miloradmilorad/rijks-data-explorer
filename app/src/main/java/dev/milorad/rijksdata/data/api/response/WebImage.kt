package dev.milorad.rijksdata.data.api.response

data class WebImage(
    val offsetPercentageY: Int?,
    val offsetPercentageX: Int?,
    val width: Int?,
    val guid: String?,
    val url: String?,
    val height: Int?
)