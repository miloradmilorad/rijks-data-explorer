package dev.milorad.rijksdata.data.converter

import dev.milorad.rijksdata.data.api.response.ArtCollectionResponse
import dev.milorad.rijksdata.data.api.response.ArtObjectDetailResponse
import dev.milorad.rijksdata.entity.ArtObject
import dev.milorad.rijksdata.entity.ArtObjectDetail

object ArtObjectResponseConverter {

    fun convert(response: ArtCollectionResponse): List<ArtObject> {
        return response.artObjects.map {
            with(it) {
                ArtObject(
                    objectNumber,
                    title,
                    principalOrFirstMaker,
                    webImage?.url,
                    headerImage?.url
                )
            }
        }
    }

    fun convert(response: ArtObjectDetailResponse): ArtObjectDetail {
        return with(response.artObject) {
            ArtObjectDetail(
                ArtObject(
                    objectNumber,
                    title,
                    principalOrFirstMaker,
                    webImage?.url,
                    null
                ),
                description
            )
        }
    }

}