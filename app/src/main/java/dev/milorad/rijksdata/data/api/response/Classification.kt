package dev.milorad.rijksdata.data.api.response

data class Classification(
    val iconClassIdentifier: List<String?>?,
    val places: List<String?>?,
    val motifs: List<Any?>?,
    val periods: List<Any?>?,
    val objectNumbers: List<String?>?,
    val people: List<String?>?,
    val events: List<Any?>?,
    val iconClassDescription: List<String?>?
)