package dev.milorad.rijksdata.data.api.response

data class ArtObjectDetailResponse(
    val artObjectPage: ArtObjectPage?,
    val artObject: ArtObject,
    val elapsedMilliseconds: Int?
)