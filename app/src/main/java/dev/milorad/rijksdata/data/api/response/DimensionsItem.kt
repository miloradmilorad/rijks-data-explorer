package dev.milorad.rijksdata.data.api.response

data class DimensionsItem(
    val unit: String?,
    val part: Any?,
    val type: String?,
    val value: String?
)