package dev.milorad.rijksdata.data.service

import androidx.annotation.IntRange
import dev.milorad.rijksdata.data.api.response.ArtCollectionResponse
import dev.milorad.rijksdata.data.api.response.ArtObjectDetailResponse
import dev.milorad.rijksdata.entity.SortMode
import io.reactivex.Observable
import io.reactivex.Single

/**
 * A Remote Service providing information about art collections.
 */
interface ArtCollectionService {

    fun getArtCollection(
        @IntRange(from = 0) page: Int,
        @IntRange(from = 1, to = 100) pageSize: Int,
        @SortMode sort: String
    ): Observable<ArtCollectionResponse>

    fun getArtObjectDetail(id: String): Single<ArtObjectDetailResponse>

}