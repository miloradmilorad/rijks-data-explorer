package dev.milorad.rijksdata.data.api.response

data class Dating(
    val period: Int?,
    val sortingDate: Int?,
    val yearLate: Int?,
    val yearEarly: Int?,
    val presentingDate: String?
)