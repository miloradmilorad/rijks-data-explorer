package dev.milorad.rijksdata.data.api.response

data class FacetsItem(
    val prettyName: Int?,
    val otherTerms: Int?,
    val name: String?,
    val facets: List<FacetsItem?>?,
    val value: Int?,
    val key: String?
)