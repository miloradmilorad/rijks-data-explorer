package dev.milorad.rijksdata.data.api.response

data class ArtObjectPage(
    val similarPages: List<Any?>?,
    val audioFileLabel2: Any?,
    val adlibOverrides: AdlibOverrides?,
    val objectNumber: String?,
    val audioFileLabel1: Any?,
    val id: String?,
    val plaqueDescription: String?,
    val audioFile1: Any?,
    val updatedOn: String?,
    val lang: String?,
    val createdOn: String?,
    val tags: List<Any?>?
)