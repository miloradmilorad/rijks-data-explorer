package dev.milorad.rijksdata.data.api.response

data class CountFacets(
    val ondisplay: Int?,
    val hasimage: Int?
)