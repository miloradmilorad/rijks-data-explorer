package dev.milorad.rijksdata.data.api.response

data class Links(
    val web: String?,
    val self: String?
)