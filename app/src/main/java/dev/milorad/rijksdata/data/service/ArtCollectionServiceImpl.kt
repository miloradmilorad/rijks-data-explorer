package dev.milorad.rijksdata.data.service

import androidx.annotation.IntRange
import dev.milorad.rijksdata.BuildConfig
import dev.milorad.rijksdata.data.api.Culture
import dev.milorad.rijksdata.data.api.RijksDataApi
import dev.milorad.rijksdata.data.api.response.ArtCollectionResponse
import dev.milorad.rijksdata.data.api.response.ArtObjectDetailResponse
import dev.milorad.rijksdata.entity.SortMode
import io.reactivex.Observable
import io.reactivex.Single

/**
 * A [ArtCollectionService] using data from the [RijksDataApi].
 */
class ArtCollectionServiceImpl(private val rijksDataApi: RijksDataApi) : ArtCollectionService {

    private companion object {
        // TODO Allow the language to be selected
        private const val DEFAULT_CULTURE = Culture.NL
    }

    override fun getArtCollection(
        @IntRange(from = 0) page: Int,
        @IntRange(from = 1, to = 100) pageSize: Int,
        @SortMode sort: String
    ): Observable<ArtCollectionResponse> {
        return rijksDataApi.getArtCollection(DEFAULT_CULTURE, page, pageSize, sort, BuildConfig.API_KEY)
    }

    override fun getArtObjectDetail(id: String): Single<ArtObjectDetailResponse> {
        return rijksDataApi.getArtObjectDetail(DEFAULT_CULTURE, id, BuildConfig.API_KEY)
    }

}