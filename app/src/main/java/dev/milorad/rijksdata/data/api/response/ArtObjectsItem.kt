package dev.milorad.rijksdata.data.api.response

data class ArtObjectsItem(
    val principalOrFirstMaker: String,
    val webImage: WebImage?,
    val headerImage: HeaderImage?,
    val objectNumber: String,
    val productionPlaces: List<Any?>?,
    val links: Links?,
    val hasImage: Boolean?,
    val showImage: Boolean?,
    val id: String?,
    val title: String,
    val longTitle: String?,
    val permitDownload: Boolean?
)