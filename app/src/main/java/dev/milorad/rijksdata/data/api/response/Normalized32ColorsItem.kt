package dev.milorad.rijksdata.data.api.response

data class Normalized32ColorsItem(
    val percentage: Int?,
    val hex: String?
)