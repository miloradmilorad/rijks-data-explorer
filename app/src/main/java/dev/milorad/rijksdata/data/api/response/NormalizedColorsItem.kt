package dev.milorad.rijksdata.data.api.response

data class NormalizedColorsItem(
    val percentage: Int?,
    val hex: String?
)