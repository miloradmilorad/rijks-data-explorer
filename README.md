
Architecture Overview:
------------

* The structure is aiming to follow Clean Architecture by splitting code into Business, Data, Entity, Presentation and
 UI (for android specific stuff). These would be moved into their own modules to enforce the separation at compile time
 but it was overkill for this use case.
* For the UI layer the app follows the MVVM pattern, relying on the android data binding for the binding part.

Domain and main dependencies:
------------

* Main goal was to be as SOLID as possible.
* The `DependencyLocator` serves as a compile-time service locator, to allow for a proper DI solution to be applied in
 the future.
* `androidx.lifecycle.ViewModel` and `androidx.databinding.Observable` are leveraged heavily to reduce boilerplate.
* RxJava2 is used to represent asynchronicity.
* Picasso for image loading.

Testing:
------------

* Unit tests only, the coverage is pretty basic but so is the app.

### TODOs ###

* Better handling of image loading, setting up Picasso to show a loading indicator while downloading images
* Pagination
* Tests for `ArtObjectCollectionViewModel`
* Better error handling for every layer
* Loading data optimization, loading once when creating the view model, not reloading when view recreated
* Caching of previously downloaded data to a local SqlLite db
* Ui work